// console.log("Hello Harold goodluckkkk!")

/*
    1. Declare 3 variables without initialization called username,password and role.
*/
	let username;
	let password;
	let role;

	function login(){
		username = prompt("Enter your username:");
		password = prompt("Enter your password:");
		role = prompt("Enter your role:");
		console.log(username);
		console.log(password);
		console.log(role);

		if(username === ""){
			alert("Input must not be empty.");
		}	
		if(password === ""){
			alert("Input must not be empty.");
		}	
		if(role === ""){
			alert("Input must not be empty.");
		}	
		else
		user = prompt("Please input your role:")
			switch (user){
		case 'admin':
			console.log("Welcome back to the class portal, admin!");
			break;
		case 'teacher':
			console.log("Thank you for logging in, teacher!")
			break;
		case 'rookie':
			console.log("Welcome to the class portal, student!")
			break;
		default:
			console.log("Role out of range.")
			break;
		}
	}
	login()

		
/*
    
    2. Create a login function which is able to prompt the user to provide their username, password and role.
        -use prompt() and update the username, password and role global variable with the prompt() returned values.
        -add an if statement to check if the the username is an empty string or null or if the password is an empty string or null or if the role is an empty string or null.
            -if it is, show an alert to inform the user that their input should not be empty.
        -add an else statement. Inside the else statement add a switch to check the user's role add 3 cases and a default:
                -if the user's role is admin, show an alert with the following message:
                    "Welcome back to the class portal, admin!"
                -if the user's role is teacher, show an alert with the following message:
                    "Thank you for logging in, teacher!"
                -if the user's role is a rookie, show an alert with the following message:
                    "Welcome to the class portal, student!"
                -if the user's role does not fall under any of the cases, as a default, show a message:
                    "Role out of range."
*/
		
	let myAverage;

	function fourNumbers(ave1, ave2, ave3, ave4){
		let average = (ave1 + ave2 + ave3 + ave4) / 4;
		return average;
		}
	myAverage = fourNumbers(75,80,90,75);

	console.log("CheckAverage(75,80,90,75)")
		if(myAverage<=74){
			console.log("Hello, student, your average is: " + myAverage + ". The letter equivalent is F");
		}
		else if(myAverage >= 75 && myAverage <=79){
			console.log("Hello, student, your average is: " + myAverage + ". The letter equivalent is D");
		}
		else if(myAverage >= 80 && myAverage <=84){
			console.log("Hello, student, your average is: " + myAverage + ". The letter equivalent is C");
		}
		else if(myAverage >= 85 && myAverage <=89){
			console.log("Hello, student, your average is: " + myAverage + ". The letter equivalent is B");
		}
		else if(myAverage >= 75 && myAverage <=79){
			console.log("Hello, student, your average is: " + myAverage + ". The letter equivalent is A");
		}
		else if(myAverage >= 75 && myAverage <=79){
			console.log("Hello, student, your average is: " + myAverage + ". The letter equivalent is A+");
		}

	// myAverage = fourNumbers(75,80,90,75);
	// console.log(myAverage);



/*
    3. Create a function which is able to receive 4 numbers as arguments calculate its average and log a message for  the user about their letter equivalent in the console.
        -add parameters appropriate to describe the arguments.
        -create a new function scoped variable called average.
        -calculate the average of the 4 number inputs and store it in the variable average.
        -research the use of Math.round() and round off the value of the average variable.
            -update the average variable with the use of Math.round()
            -console.log() the average variable to check if it is rounding off first.
        -add an if statement to check if the value of average is less than or equal to 74.
            -if it is, show the following message in a console.log():
            "Hello, student, your average is <show average>. The letter equivalent is F"
        -add an else if statement to check if the value of average is greater than or equal to 75 and if average is less than or equal to 79.
            -if it is, show the following message in a console.log():
            "Hello, student, your average is <show average>. The letter equivalent is D"
        -add an else if statement to check if the value of average is greater than or equal to 80 and if average is less than or equal to 84.
            -if it is, show the following message in a console.log():
            "Hello, student, your average is <show average>. The letter equivalent is C"
        -add an else if statement to check if the value of average is greater than or equal to 85 and if average is less than or equal to 89.
            -if it is, show the following message in a console.log():
            "Hello, student, your average is <show average>. The letter equivalent is B"
        -add an else if statement to check if the value of average is greater than or equal to 90 and if average is less than or equal to 95.
            -if it is, show the following message in a console.log():
            "Hello, student, your average is <show average>. The letter equivalent is A"
        -add an else if statement to check if the value of average is greater than 96.
            -if it is, show the following message in a console.log():
            "Hello, student, your average is <show average>. The letter equivalent is A+"

        Invoke and add a number as argument using the browser console.

        Stretch goal:
            -Add an if statement to check the role of the user.
                -if the role of the user is currently "teacher" or "admin" or undefined or null, show an alert:
                "<role>! You are not allowed to access this feature!".
                -else, continue to evaluate the letter equivalent of the student's average.

*/



































